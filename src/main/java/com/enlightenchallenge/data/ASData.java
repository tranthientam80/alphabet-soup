package com.enligtenchallenge.data;


import java.util.ArrayList;
import java.util.LinkedHashMap;


/**
 * Description: This is the data structure containing the parsed
 *  data from the user input text file. Objects created from this class
 *  contains runtime data that can be used to solve the find-word puzzle
 *  from the user input text file. 
 */
public class ASData {
  private final ASGrid grid;
  private final ArrayList<ASWord> words; // the list of words to search
  private final LinkedHashMap<String, ArrayList<ASGridIndex>> left2Right;
  private final LinkedHashMap<String, ArrayList<ASGridIndex>> top2Bottom;
  private final LinkedHashMap<String, ArrayList<ASGridIndex>> topLeft2BottomRight;
  private final LinkedHashMap<String, ArrayList<ASGridIndex>> topRight2BottomLeft;

  /**
   * Description: The runtime data constructor.
   * 
   * @param grid - Contains the find-word puzzle cartesian matrix and its
   *                associated dimension
   * @param words - The list of words to find in the find-word puzzle
   * @param left2Right - list of all horizontal strings on the grid
   * @param top2Bottom - list of all vertical strings on the grid
   * @param topLeft2BottomRight - list of all negative slope diagonal strings on the grid
   * @param topRight2BottomLeft - list of all positive slope diagonal strings on the grid
   */
  public ASData(
    final ASGrid grid,
    final ArrayList<ASWord> words,
    final LinkedHashMap<String, ArrayList<ASGridIndex>> left2Right,
    final LinkedHashMap<String, ArrayList<ASGridIndex>> top2Bottom,
    final LinkedHashMap<String, ArrayList<ASGridIndex>> topLeft2BottomRight,
    final LinkedHashMap<String, ArrayList<ASGridIndex>> topRight2BottomLeft)
  {
    this.grid = grid;
    this.words = words;
    this.left2Right = left2Right;
    this.top2Bottom = top2Bottom;
    this.topLeft2BottomRight = topLeft2BottomRight;
    this.topRight2BottomLeft = topRight2BottomLeft;
  }

  /**
   * Description: Provide access to the cartesian find-word grid.
   * 
   * @return - The ASGrid object for reference access
   */
  public ASGrid grid() {
    return grid;
  }

  /**
   * Description: Provide access to the list of word to find.
   * 
   * @return - The list of ASWord for reference access
   */
  public ArrayList<ASWord> words() {
    return words;
  }

  /**
   * Description: Provide access to the list of horizontal strings on the grid.
   * 
   * @return - The list of grid horizontal strings and accosiated indices reference
   */
  public LinkedHashMap<String, ArrayList<ASGridIndex>> left2Right() {
    return this.left2Right;
  }

  /**
   * Description: Provide access to the list of vertical strings on the grid.
   * 
   * @return - The list of grid vertical strings and accosiated indices reference
   */
  public LinkedHashMap<String, ArrayList<ASGridIndex>> top2Bottom() {
    return this.top2Bottom;
  }

  /**
   * Description: Provide access to the list of negative slope diagonal strings
   *  on the grid.
   * 
   * @return - The list of grid negative slope diagonal strings and accosiated
   *            indices reference
   */
  public LinkedHashMap<String, ArrayList<ASGridIndex>> topLeft2BottomRight() {
    return this.topLeft2BottomRight;
  }

  /**
   * Description: Provide access to the list of positive slope diagonal strings
   *  on the grid.
   * 
   * @return - The list of grid positive slope diagonal strings and accosiated
   *            indices reference
   */
  public LinkedHashMap<String, ArrayList<ASGridIndex>> topRight2BottomLeft() {
    return this.topRight2BottomLeft;
  }
}
package com.enligtenchallenge.data;


/**
 * Description: This class is an extension ASGridIndex, which uses it to
 *  represent the grid dimension. This class holds the cartesian matrix
 *  of chars representing the find-word grid puzzle.  
 */
public class ASGrid extends ASGridIndex {
  private final char[][] data;

  /**
   * Description: The constructor initializes the super class ASGridIndex
   *  with its grid dimension and initializes the grid matrix array
   */
  public ASGrid(final ASGridIndex dimension, final char[][] data) {
    super(dimension);
    this.data = data;
  }

  /**
   * Description: Provides access to the chars on the chars matrix array
   * 
   * @param row - The integer value representing row position on the grid
   * @param column - The integer value representing the column position on the grid
   * @return - The character value on the grid at row by column
   */
  public char at(final int row, final int col) {
    return data[row][col];
  }
}
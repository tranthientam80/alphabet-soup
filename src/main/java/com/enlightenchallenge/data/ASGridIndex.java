package com.enligtenchallenge.data;


/**
 * Description: Helper class used to represent an index for a cartesian grid,
 *  rather than using a primitive array.
 */
public class ASGridIndex {
  protected final int row;
  protected final int column;

  /**
   * Description: The ASGridIndex constructor that accepts a row
   *  and column values
   * 
   * @param row - an integer row value
   * @param column - an integer column value
   */
  public ASGridIndex(final int row, final int column) {
    this.row = row;
    this.column = column;
  }

  /**
   * Description: The ASGridIndex constructor that accepts an array
   * 
   * @param array - an array that contains
   */
  public ASGridIndex(final int[] array) {
    this(array[0], array[1]);
  }

  /**
   * Description: Copy constructor
   * 
   * @param that - that is the other ASGridIndex object
   */
  public ASGridIndex(final ASGridIndex that) {
    this(that.row, that.column);
  }

  /**
   * Description: Provides access to the internal row field member
   * 
   * @return - An integer row value
   */
  public int row() {
    return row;
  }

  /**
   * Description: Provides access to the internal column field member
   * 
   * @return - An integer column value
   */
  public int column() {
    return column;
  }

  /**
   * Description: Provides a string view to the object row and
   *  column field members
   * 
   * @return - string view to the row and column field members
   */
  public String toString() {
    return row + ":" + column;
  }
}
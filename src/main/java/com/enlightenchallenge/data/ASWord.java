package com.enligtenchallenge.data;


/**
 * Description: Represents a single word to search in the find-word puzzle.
 *  If the word exists in the puzzle, it will store the starting and ending
 *  index of where that word is located on the find-word grid. It can also
 *  contains mechanism to reverse of its own word as well.
 */
public class ASWord {
  private final String word;
  private final String reverseWord;
  private boolean reverseEnabled; // toggle word in reverse mode 
  private ASGridIndex start;
  private ASGridIndex end;
  
  /**
   * Description: The ASWord constructor. Just initializes field members.
   * 
   * @param word - The string containing the word that this object represent
   */
  public ASWord(final String word) {
    this.word = word;
    this.reverseWord = new StringBuilder(word).reverse().toString();
    reverseEnabled = false;
    start = null;
    end = null;
  }

  /**
   * Description: Provides a string view of the word indices if any exists
   * 
   * @return - The string representation of the word indices if it is found
   *            on the grid
   */
  public String getIndices() {
    return (start == null || end == null) ? null : start + " " + end;
  }

  /**
   * Description: Sets the indices for the word is it is found on the grid
   * 
   * @param start - The start indices of where the word is found on the grid
   * @param end - The end indices of there the word is found on the grid
   */
  public void setIndices(final ASGridIndex start, final ASGridIndex end) {
    if(reverseEnabled) {
      this.start = end;
      this.end = start;
    } else {
      this.start = start;
      this.end = end;
    }
  }

  /**
   * Description: Toggle reverse word mode. Used to search the word backward
   *              on the grid
   */
  public void reverse(final boolean state) {
    reverseEnabled = state;
  }

  /**
   * Description: Provide a view to the length of the internal word string
   * 
   * @return - The length of the internal word string
   */
  public int length() {
    return word.length();
  }

  /**
   * Description: Provide access to the internal word string in either naturally
   *  or reverse format
   * 
   * @return - The string containing the word in either natural format or reverse
   *            format depending on mode
   */
  public String toString() {
    if(reverseEnabled) {
      return reverseWord;
    } else {
      return word;
    }
  }
}
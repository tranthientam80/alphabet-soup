package com.enligtenchallenge.utils;


import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;

import com.enligtenchallenge.data.ASData;
import com.enligtenchallenge.data.ASGrid;
import com.enligtenchallenge.data.ASWord;
import com.enligtenchallenge.data.ASGridIndex;


/**
 * Description: This is a utility class. It mostly contains the parser mechanisms
 *  used to assists the AlphabetSoup application to convert user input text data
 *  into runtime data that which it can use to solve the find-word puzzle  
 */
public class ASUtils {
  /**
   * Description: This method parses the user input text file into runtime data
   *  and stores them into an ASData object for the AlphabetSoup application to use.
   *  The ASData object is effectively a runtime representation of the user input
   *  text file, but structured in a way that the AlphabetSoup application can use.
   */
  public static ASData parseFile(final File file) throws IOException {
    BufferedReader br = new BufferedReader(new FileReader(file));
    ArrayList<ASWord> words = new ArrayList<>();
    ASGridIndex dimension = null;
    ASGrid grid = null;
    char[][] matrix = null;
    LinkedHashMap<String, ArrayList<ASGridIndex>> left2Right = new LinkedHashMap<>();
    LinkedHashMap<String, ArrayList<ASGridIndex>> top2Bottom = new LinkedHashMap<>();
    LinkedHashMap<String, ArrayList<ASGridIndex>> topLeft2BottomRight = new LinkedHashMap<>();
    LinkedHashMap<String, ArrayList<ASGridIndex>> topRight2BottomLeft = new LinkedHashMap<>();

    String line = null;
    int i = 0;
    while((line = br.readLine()) != null) {
      if(0 == i) {
        dimension = new ASGridIndex(Arrays.stream(line.split("x")).mapToInt(Integer::parseInt).toArray());
        matrix = new char[dimension.row()][dimension.column()];
      } else if(i > 0 && i < dimension.row() + 1) {
        matrix[i - 1] = line.replaceAll(" ", "").toCharArray();
      } else {
        words.add(new ASWord(line.replaceAll(" ","")));
      }
      ++i;
    }

    grid = new ASGrid(dimension, matrix);

    parseGridLeft2Right(grid, left2Right);
    parseGridTop2Bottom(grid, top2Bottom);
    parseGridTopLeft2BottomRight(grid, topLeft2BottomRight);
    parseGridTopRight2BottomLeft(grid, topRight2BottomLeft);

    return new ASData(grid, words, left2Right, top2Bottom, topLeft2BottomRight, topRight2BottomLeft);
  }

  /**
   * Description: Find a word in a list of similar patern strings. If found
   *  it sets the starting and ending indices on the grid for that word.
   * 
   * @param word - object containg the word to search
   * @param data - a list containg similar patern string and its associated indices
   *                referencing where each character are located on the grid puzzle
   * @return boolean - true if the word is found on the list, false otherwise 
   */
  public static boolean findWord(final ASWord word, final LinkedHashMap<String, ArrayList<ASGridIndex>> data) {
    for(final String str : data.keySet()) {
      int leftIdx = str.indexOf(word.toString());
      if(leftIdx >= 0) {
        int rightIdx = leftIdx + word.length();
        ASGridIndex start = data.get(str).get(leftIdx);
        ASGridIndex end = data.get(str).get(rightIdx - 1);
        word.setIndices(start, end);
        return true;
      }
    }
    return false;
  }

  /**
   * Description: A Helper mechanism for the parseFile method to use in parsing the
   *  find-word grid into runtime data structure. Left2Right means that it parses
   *  all horizontal rows of texts on the grid into individual strings and associates
   *  lists of indices of where each character on that string is located on the grid
   * 
   * @param grid - The object containing the find-word puzzle grid in a cartesian
   *                matrix of chars
   * @param left2Right - The data structure used to store the parsed grid data 
   */
  private static void parseGridLeft2Right(final ASGrid grid, LinkedHashMap<String, ArrayList<ASGridIndex>> left2Right) {
    for(int row = 0; row < grid.row(); ++row) {
      StringBuilder sb = new StringBuilder();
      ArrayList<ASGridIndex> al = new ArrayList<>();
      for(int col = 0; col < grid.column(); ++col) {
        sb.append(grid.at(row, col));
        al.add(new ASGridIndex(row, col));
      }
      left2Right.put(sb.toString(), al);
    }
  }

  /**
   * Description: A Helper mechanism for the parseFile method to use in parsing the
   *  find-word grid into runtime data structure. Top2Botom means that it parses
   *  all vertical column of texts on the grid into individual strings and associates
   *  lists of indices of where each character on that string is located on the grid
   * 
   * @param grid - The object containing the find-word puzzle grid in a cartesian
   *                matrix of chars
   * @param top2Bottom - The data structure used to store the parsed grid data 
   */
  private static void parseGridTop2Bottom(final ASGrid grid, LinkedHashMap<String, ArrayList<ASGridIndex>> top2Bottom) {
    for(int col = 0; col < grid.column(); ++col) {
      StringBuilder sb = new StringBuilder();
      ArrayList<ASGridIndex> al = new ArrayList<>();
      for(int row = 0; row < grid.row(); ++row) {
        sb.append(grid.at(row, col));
        al.add(new ASGridIndex(row, col));
      }
      top2Bottom.put(sb.toString(), al);
    }
  }

  /**
   * Description: A Helper mechanism for the parseFile method to use in parsing the
   *  find-word grid into runtime data structure. TopLeft2BottomRight means that it
   *  parses all negative slope strings of chars on the grid into individual strings
   *  and associates lists of indices of where each character on that string is
   *  located on the grid
   * 
   * @param grid - The object containing the find-word puzzle grid in a cartesian
   *                matrix of chars
   * @param topLeft2BottomRight - The data structure used to store the parsed grid data 
   */
  private static void parseGridTopLeft2BottomRight(final ASGrid grid, LinkedHashMap<String, ArrayList<ASGridIndex>> topLeft2BottomRight) {
    for(int top = 0; top < grid.row(); ++top) {
      StringBuilder sb = new StringBuilder();
      ArrayList<ASGridIndex> al = new ArrayList<>();
      for(int row = top, col = 0; col < grid.column() && row >= 0; --row, ++col) {
        sb.append(grid.at(row, col));
        al.add(new ASGridIndex(row, col));
      }
      topLeft2BottomRight.put(sb.toString(), al);
    }

    for(int botom = 1; botom < grid.column(); ++botom) {
      StringBuilder sb = new StringBuilder();
      ArrayList<ASGridIndex> al = new ArrayList<>();
      for(int row = grid.row() - 1, col = botom; col < grid.column() && row >= 0; --row, ++col) {
        sb.append(grid.at(row, col));
        al.add(new ASGridIndex(row, col));
      }
      topLeft2BottomRight.put(sb.toString(), al);
    }
  }

  /**
   * Description: A Helper mechanism for the parseFile method to use in parsing the
   *  find-word grid into runtime data structure. TopRight2BottmLeft means that it
   *  parses all positive slope strings of chars on the grid into individual strings
   *  and associates lists of indices of where each character on that string is
   *  located on the grid
   * 
   * @param grid - The object containing the find-word puzzle grid in a cartesian
   *                matrix of chars
   * @param topRight2BottmLeft - The data structure used to store the parsed grid data 
   */
  private static void parseGridTopRight2BottomLeft(final ASGrid grid, LinkedHashMap<String, ArrayList<ASGridIndex>> topRight2BottmLeft) {
    for(int top = 0; top < grid.row(); ++top) {
      StringBuilder sb = new StringBuilder();
      ArrayList<ASGridIndex> al = new ArrayList<>();
      for(int row = top, col = grid.column() - 1; col >= 0 && row >= 0; --row, --col) {
        sb.append(grid.at(row, col));
        al.add(new ASGridIndex(row, col));
      }
      topRight2BottmLeft.put(sb.toString(), al);
    }

    for(int botom = grid.column() - 1; botom >= 0; --botom) {
      StringBuilder sb = new StringBuilder();
      ArrayList<ASGridIndex> al = new ArrayList<>();
      for(int row = grid.row() - 1, col = botom - 1; col >= 0 && row >= 0; --row, --col) {
        sb.append(grid.at(row, col));
        al.add(new ASGridIndex(row, col));
      }
      topRight2BottmLeft.put(sb.toString(), al);
    }
  }
}
package com.enligtenchallenge.app;


import java.util.Scanner;
import java.util.List;
import java.util.Collections;
import java.util.Set;
import java.io.File;
import java.io.IOException;

import com.enligtenchallenge.utils.ASUtils;
import com.enligtenchallenge.data.ASData;
import com.enligtenchallenge.data.ASGrid;
import com.enligtenchallenge.data.ASGridIndex;
import com.enligtenchallenge.data.ASWord;


/**
 * Description: 
 */
public class AlphabetSoup {
  private final ASData data;

  /**
   * Description: The AlphabetSoup class constructor. Initializes ASData object
   *  needed for the application to work
   * 
   * @param data - The ASData object containing the fully parsed data from the
   *                user input text file 
   */
  private AlphabetSoup(final ASData data) {
    this.data = data;
  }

  /**
   * Description: The run method does the word puzzle solving procedures,
   *  provided the input text file contains no errors
   */
  private void run() {
    for(final ASWord word : data.words()) {
      if(ASUtils.findWord(word, data.left2Right())) {
        continue;
      } else if(ASUtils.findWord(word, data.top2Bottom())) {
        continue;
      } else if(ASUtils.findWord(word, data.topLeft2BottomRight())) {
        continue;
      } else if(ASUtils.findWord(word, data.topRight2BottomLeft())) {
        continue;
      } else {
        word.reverse(true);
      }
      
      if(ASUtils.findWord(word, data.left2Right())) {
        continue;
      } else if(ASUtils.findWord(word, data.top2Bottom())) {
        continue;
      } else if(ASUtils.findWord(word, data.topLeft2BottomRight())) {
        continue;
      } else if(ASUtils.findWord(word, data.topRight2BottomLeft())) {
        continue;
      }
    }

    for(final ASWord word : data.words()) {
      if(word.getIndices() != null) {
        word.reverse(false);
        System.out.println(word + " " + word.getIndices());
      }
    }
  }

  /**
   * Description: The entry point of the AlphabetSoup application. It sets up the
   *  input stream needed to accept user input for the text file containing the
   *  word puzzle grid, its dimension and the words to find in the puzzle. It then
   *  uses a ASUtils to parse the user input text file into ASData object needed
   *  to run the application
   */
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Please enter input file: ");

    try {
      ASData data = ASUtils.parseFile(new File(scanner.nextLine()));
      AlphabetSoup alphabetSoup = new AlphabetSoup(data);
      alphabetSoup.run();
    } catch(final IOException e) {
      e.printStackTrace();
      System.exit(1);
    }
  }
}